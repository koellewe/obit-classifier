# OBIT Classifier
> **O**ntology-**B**ased **I**nsider **T**hreat Classifier

OBIT is a program that parses [SOFIT](https://ieeexplore.ieee.org/abstract/document/8424651) -compatible ontologies to classify insider threats. 

Watch a [quick intro video](https://videos.rauten.co.za/w/w5mDwLnqBDozZcajW6khoG).

## Usage

```sh
./obit -o sofit.owl 
```

For details, see the [Usage wiki entry](https://gitlab.com/J4NS-R/obit-classifier/-/wikis/Usage).

## Building

See the [Building wiki entry](https://gitlab.com/J4NS-R/obit-classifier/-/wikis/Building).

## Everything else

Check the [wiki](https://gitlab.com/J4NS-R/obit-classifier/-/wikis/home).
