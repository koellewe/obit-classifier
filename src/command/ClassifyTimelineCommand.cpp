
#include <jsoncons/json.hpp>
#include "ClassifyTimelineCommand.h"
#include "../format/TableFormatter.h"
#include "../format/CSVFormatter.h"

void ClassifyTimelineCommand::executeAndPrint(const string &format) {
    list<TimelineThreatAnalysis> anals;

    for(auto& emplPair : timeline->getTimelineByEmployee()){
        auto res = classifier.classifyTimeline(emplPair.first, emplPair.second);
        anals.push_back(res);
    }

    if (format=="table"){
        vector<pair<string, int>> headers{
                pair("Employee", 20),
                pair("Risk total", 12),
                pair("Classification", 15),
                pair("Incidents", 10),
                pair("Analysis period", 22),
        };
        list<vector<string>> data;
        for(auto& anal : anals){
            vector<string> row{
                anal.getEmployee(),
                std::to_string(anal.getRiskTotal()),
                ThreatAnalysis::threatToString(anal.getThreatLevel()),
                std::to_string(anal.getIncidents().size()),
                prettifyDuration(anal.getDurationDays(runToToday)),
            };
            data.push_back(row);
        }

        TableFormatter formatter(headers, data);
        formatter.formatAndPrint();

    }else if (format=="csv"){
        list<string> headers{
            "Employee", "Risk_total", "Classification", "Incident_count", "Analysis_period_days"
        };
        list<list<string>> data;
        for(auto& anal : anals){
            list<string> row{
                anal.getEmployee(),
                std::to_string(anal.getRiskTotal()),
                ThreatAnalysis::threatToString(anal.getThreatLevel()),
                std::to_string(anal.getIncidents().size()),
                std::to_string(anal.getDurationDays(runToToday)),
            };
            data.push_back(row);
        }

        CSVFormatter formatter(headers, data);
        formatter.formatAndPrint();

    }else if (format=="json"){
        jsoncons::json root;
        for(auto& anal : anals){
            jsoncons::json emplNode;
            emplNode["risk_total"] = anal.getRiskTotal();
            emplNode["classification"] = ThreatAnalysis::threatToString(anal.getThreatLevel());
            emplNode["analysis_period_days"] = anal.getDurationDays(runToToday);
            jsoncons::json incidents(jsoncons::json_array_arg);
            for(auto& incident : anal.getIncidents()){
                jsoncons::json incidentNode;
                incidentNode["date_ts"] = incident.first.getSecs();
                incidentNode["date_str"] = incident.first.toDateStr();
                incidentNode["factor"] = incident.second->getIri();
                incidents.push_back(incidentNode);
            }
            emplNode["incidents"] = incidents;

            root[anal.getEmployee()] = emplNode;
        }
        // print out
        cout << jsoncons::pretty_print(root) << endl;
    }

}
