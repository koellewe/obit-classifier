
#ifndef OBIT_CLASSIFIER_CLASSIFYCOMMAND_H
#define OBIT_CLASSIFIER_CLASSIFYCOMMAND_H


#include "../owl/ThreatClassifier.h"

class ClassifyCommand {
private:
    ThreatClassifier classifier;

public:
    ClassifyCommand(SOFITParser* sofit, ConfigParser& cfg) : classifier(sofit, cfg){}

    void executeAndPrint(string& format);
};


#endif //OBIT_CLASSIFIER_CLASSIFYCOMMAND_H
