
#include "TimelineCommand.h"
#include "../format/TableFormatter.h"
#include "../format/CSVFormatter.h"
#include <vector>
#include <string>
#include <jsoncons/json.hpp>

using namespace std;

void TimelineCommand::executeAndPrint(const string &format) {
    auto empls = timeline->getTimelineByEmployee();

    if (format=="table"){
        vector<pair<string, int>> headers{
                pair("Employee", 20),
                pair("Date", 12),
                pair("Factor", 28),
                pair("Running risk total", 18),
                pair("Classification", 14),
        };
        list<vector<string>> data;
        vector<int> separators;
        for(auto& empl : empls) {
            // add sep at the beginning of a new employee
            if (empl != *empls.begin()){
                separators.push_back(data.size());
            }

            TimelineThreatAnalysis anal(empl.first);
            Date* lastIncident = nullptr;
            for(auto& datePair : empl.second) {
                // do step classification
                if (lastIncident == nullptr)
                    classifier.classifyRunningTimeline(&anal, datePair.first, datePair.second);
                else{
                    int daysSinceLastIncident = dayDiff(*lastIncident, datePair.first);
                    classifier.classifyRunningTimeline(&anal, datePair.first, datePair.second, daysSinceLastIncident);
                }
                vector<string> row{
                        datePair == *empl.second.begin() ? empl.first : "",  // only print empl name once
                        datePair.first.toDateStr(),
                        datePair.second->getIri(),
                        to_string(anal.getRiskTotal()),
                        ThreatAnalysis::threatToString(anal.getThreatLevel()),
                };

                lastIncident = &datePair.first;
                data.push_back(row);
            }
            // handle runToToday row
            if (runToToday){
                classifier.classifyToToday(&anal, *lastIncident);
                vector<string> row{
                    "",
                    (new Date)->toDateStr(), // today's date
                    "[Today]",
                    to_string(anal.getRiskTotal()),
                    ThreatAnalysis::threatToString(anal.getThreatLevel()),
                };
                data.push_back(row);
            }
        }

        //print out
        TableFormatter formatter(headers, data);
        formatter.setSeparatorIndices(separators);
        formatter.formatAndPrint();

    }else if (format == "csv"){
        list<string> headers{
                "Employee", "Date", "Factor", "Running_risk_total", "Classification",
        };
        list<list<string>> data;
        for(auto& empl : empls){

            TimelineThreatAnalysis anal(empl.first);
            Date* lastIncident = nullptr;
            for(auto& datePair : empl.second){
                // step classification
                int daysSinceLast = lastIncident == nullptr ? 0 :
                        dayDiff(*lastIncident, datePair.first);
                classifier.classifyRunningTimeline(&anal, datePair.first, datePair.second, daysSinceLast);
                list<string> row{
                        empl.first,
                        datePair.first.toDateStr(),
                        datePair.second->getIri(),
                        to_string(anal.getRiskTotal()),
                        ThreatAnalysis::threatToString(anal.getThreatLevel()),
                };

                lastIncident = &datePair.first;
                data.push_back(row);
            }
            // handle runToToday row
            if (runToToday){
                classifier.classifyToToday(&anal, *lastIncident);
                list<string> row{
                        empl.first,
                        (new Date)->toDateStr(), // today's date
                        "[Today]",
                        to_string(anal.getRiskTotal()),
                        ThreatAnalysis::threatToString(anal.getThreatLevel()),
                };
                data.push_back(row);
            }
        }

        // print out
        CSVFormatter formatter(headers, data);
        formatter.formatAndPrint();

    }else if (format == "json"){
        jsoncons::json root;
        for(auto& empl : empls){
            jsoncons::json incidentList(jsoncons::json_array_arg);

            TimelineThreatAnalysis anal(empl.first);
            Date* lastIncident = nullptr;
            for(auto& datePair : empl.second){
                jsoncons::json incidentNode;
                // step classification
                int daysSinceLast = lastIncident == nullptr ? 0 :
                                    dayDiff(*lastIncident, datePair.first);
                classifier.classifyRunningTimeline(&anal, datePair.first, datePair.second, daysSinceLast);

                incidentNode["date_ts"] = datePair.first.getSecs();
                incidentNode["date_str"] = datePair.first.toDateStr();
                incidentNode["factor"] = datePair.second->getIri();
                incidentNode["running_risk_total"] = anal.getRiskTotal();
                incidentNode["classification"] = ThreatAnalysis::threatToString(anal.getThreatLevel());

                lastIncident = &datePair.first;
                incidentList.push_back(incidentNode);
            }
            // handle runToToday
            if (runToToday){
                classifier.classifyToToday(&anal, *lastIncident);
                jsoncons::json incidentNode;
                Date today;
                incidentNode["date_ts"] = today.getSecs();
                incidentNode["date_str"] = today.toDateStr();
                incidentNode["factor"] = "[today]";
                incidentNode["running_risk_total"] = anal.getRiskTotal();
                incidentNode["classification"] = ThreatAnalysis::threatToString(anal.getThreatLevel());
                incidentList.push_back(incidentNode);
            }

            root[empl.first] = incidentList;
        }

        // print out
        cout << jsoncons::pretty_print(root) << endl;
    }

}
