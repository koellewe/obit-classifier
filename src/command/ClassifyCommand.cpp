
#include "ClassifyCommand.h"
#include "../format/TableFormatter.h"
#include "../format/CSVFormatter.h"
#include <vector>
#include <jsoncons/json.hpp>
#include <iostream>

void ClassifyCommand::executeAndPrint(string &format) {
    classifier.classifyThreats();
    auto result = classifier.getEmplClassifications();

    // format output
    if (format == "table"){
        vector<pair<string, int>> headers{
                pair("Employee", 20),
                pair("Risk total", 12),
                pair("Classification", 15),
        };
        list<vector<string>> data;
        for (auto& emplPair : result){
            vector<string> row(3);
            row[0] = emplPair.first;
            row[1] = std::to_string(emplPair.second->getRiskTotal());
            row[2] = ThreatAnalysis::threatToString(emplPair.second->getThreatLevel());
            data.push_back(row);
        }
        // print out
        TableFormatter formatter(headers, data);
        formatter.formatAndPrint();

    }else if (format == "csv"){
        list<string> headers{"Employee","Risk_total","Classification"};
        list<list<string>> data;
        for(auto& emplPair : result){
            list<string> row{
                    emplPair.first,
                    std::to_string(emplPair.second->getRiskTotal()),
                    ThreatAnalysis::threatToString(emplPair.second->getThreatLevel()),
            };
            data.push_back(row);
        }
        //print out
        CSVFormatter formatter(headers, data);
        formatter.formatAndPrint();

    }else if (format == "json"){
        jsoncons::json root;
        for(auto& emplPair : result){
            jsoncons::json emplNode;
            emplNode["risk_total"] = emplPair.second->getRiskTotal();
            emplNode["classification"] = ThreatAnalysis::threatToString(emplPair.second->getThreatLevel());
            root[emplPair.first] = emplNode;
        }
        // print out
        cout << jsoncons::pretty_print(root) << endl;
    }

}
