
#ifndef OBIT_CLASSIFIER_CLASSIFYTIMELINECOMMAND_H
#define OBIT_CLASSIFIER_CLASSIFYTIMELINECOMMAND_H


#include "../owl/TimelineParser.h"

class ClassifyTimelineCommand {
private:
    TimelineParser* timeline;
    ThreatClassifier classifier;
    bool runToToday;

public:
    explicit ClassifyTimelineCommand(TimelineParser* timeline, SOFITParser* sofit, ConfigParser& cfg) :
        timeline(timeline), classifier(sofit, cfg){
        runToToday = cfg.getRunToToday();
    }

    void executeAndPrint(const string& format);
};


#endif //OBIT_CLASSIFIER_CLASSIFYTIMELINECOMMAND_H
