
#include "PrintEmployeesCommand.h"
#include "../format/TableFormatter.h"
#include "../format/CSVFormatter.h"
#include <jsoncons/json.hpp>
#include <vector>
#include <iostream>

using namespace std;

void PrintEmployeesCommand::executeAndPrint(string &format) {
    auto employees = sofit->getEmployeeFactors();

    if (format == "table"){
        vector<pair<string, int>> headers{
                pair("Employee", 20),
                pair("Factors", 64),
        };
        list<vector<string>> data;
        for(auto& emplPair : employees){
            vector<string> row(2);
            row[0] = emplPair.first; // empl IRI
            string factors; int i = 0;
            for(auto& fac : emplPair.second){ // iterate over factors
                factors.append(fac->getIri());
                if (i++ < emplPair.second.size()-1)
                    factors.append(",");  // do not add comma to last item
            }
            row[1] = factors;
            data.push_back(row);
        }
        // print out
        TableFormatter formatter(headers, data);
        formatter.formatAndPrint();

    }else if (format == "csv"){
        list<string> headers{"Employee", "Factors"};
        list<list<string>> data;
        for(auto& emplPair : employees){
            list<string> row;
            row.push_back(emplPair.first); // empl IRI
            for(auto& fac : emplPair.second){ // iterate over factors
                row.push_back(fac->getIri());
            }
            data.push_back(row);
        }
        // print out
        CSVFormatter formatter(headers, data);
        formatter.formatAndPrint();

    }else if (format == "json"){
        jsoncons::json root;
        for(auto& emplPair : employees){
            jsoncons:: json facList(jsoncons::json_array_arg);
            for(auto& fac : emplPair.second){
                facList.push_back(fac->getIri());
            }
            root[emplPair.first] = facList;
        }
        // print out
        cout << jsoncons::pretty_print(root) << endl;
    }

}
