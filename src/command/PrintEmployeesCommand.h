
#ifndef OBIT_CLASSIFIER_PRINTEMPLOYEESCOMMAND_H
#define OBIT_CLASSIFIER_PRINTEMPLOYEESCOMMAND_H


#include "../owl/SOFITParser.h"

class PrintEmployeesCommand {
private:
    SOFITParser* sofit;

public:
    explicit PrintEmployeesCommand(SOFITParser *sofit) : sofit(sofit) {}

    void executeAndPrint(string& format);
};


#endif //OBIT_CLASSIFIER_PRINTEMPLOYEESCOMMAND_H
