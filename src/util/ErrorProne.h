
#ifndef OBIT_CLASSIFIER_ERRORPRONE_H
#define OBIT_CLASSIFIER_ERRORPRONE_H

#include <string>

class ErrorProne {
protected:
    std::string error;

public:
    bool isError(){
        return !error.empty();
    }
    /**
     * Retrieves and resets the internal error.
     * If you don't want to reset it, use isError() first.
     * @return the error description
     */
    [[nodiscard]] std::string getResetError(){
        std::string copy = error;
        error = "";
        return copy;
    }
};


#endif //OBIT_CLASSIFIER_ERRORPRONE_H
