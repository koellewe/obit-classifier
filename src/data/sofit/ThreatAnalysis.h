
#ifndef OBIT_CLASSIFIER_THREATANALYSIS_H
#define OBIT_CLASSIFIER_THREATANALYSIS_H

#include <string>
#include <list>
#include <utility>
#include <map>

using namespace std;

class ThreatAnalysis {
public:
    enum THREAT_LEVEL{
        NONE, LOW, MEDIUM, HIGH, EXTREME
    };
    // used for index lookups
    constexpr static const THREAT_LEVEL THREAT_LEVEL_ARR[] = {
        NONE, LOW, MEDIUM, HIGH, EXTREME
    };

private:
    const string employee;
    const list<const Factor*> factors{};
    int riskTotal;
    THREAT_LEVEL threatLevel;

public:
    ThreatAnalysis(string employee, const list<const Factor *> &factors, int riskTotal, THREAT_LEVEL threatLevel)
            : employee(std::move(employee)), factors(factors), riskTotal(riskTotal), threatLevel(threatLevel){};

    // getters
    [[nodiscard]] const string &getEmployee() const {
        return employee;
    }

    [[nodiscard]] const list<const Factor *> &getFactors() const {
        return factors;
    }

    [[nodiscard]] int getRiskTotal() const {
        return riskTotal;
    }

    [[nodiscard]] THREAT_LEVEL getThreatLevel() const {
        return threatLevel;
    }

    // external utility function
    static string threatToString(THREAT_LEVEL threatLevel){
        // Try set up a static const map at init. Not easy in C++.
        if (threatLevel == NONE)
            return "None";
        else if (threatLevel == LOW)
            return "Low";
        else if (threatLevel == MEDIUM)
            return "Medium";
        else if (threatLevel == HIGH)
            return "High";
        else
            return "Extreme";
    }
};

#endif //OBIT_CLASSIFIER_THREATANALYSIS_H
