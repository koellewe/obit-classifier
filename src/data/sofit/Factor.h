
#ifndef OBIT_CLASSIFIER_FACTOR_H
#define OBIT_CLASSIFIER_FACTOR_H

#include <string>

/**
 * Simple class to hold data related to SOFIT Factors
 */
class Factor {
private:
    std::string iri;
    int riskVal = -1;
    bool enduring = false;
public:
    explicit Factor(std::string& iri){
        this->iri = iri;
    }
    Factor(std::string& iri, int riskVal){
        this->iri = iri;
        this->riskVal = riskVal;
    }

    // getters
    [[nodiscard]] std::string getIri() const {
        return iri;
    }
    [[nodiscard]] int getRiskVal() const {
        return riskVal;
    }
    [[nodiscard]] bool isEnduring() const{
        return enduring;
    }

    // setters
    void setRiskVal(int risk){
        this->riskVal = risk;
    }
    void setEnduring(){
        enduring = true;
    }

    bool operator==(const Factor& other) const{
        return iri == other.iri;
    }
};

#endif
