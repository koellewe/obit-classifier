
#ifndef OBIT_CLASSIFIER_TIMELINETHREATANALYSIS_H
#define OBIT_CLASSIFIER_TIMELINETHREATANALYSIS_H

#include "../../util/Date.h"
#include "Factor.h"
#include <utility>
#include <vector>
#include <string>
#include "ThreatAnalysis.h"
#include "../../util/utils.h"

using namespace std;
typedef ThreatAnalysis::THREAT_LEVEL THREAT_LEVEL;

class TimelineThreatAnalysis {
private:
    const string employee;
    vector<pair<Date, const Factor*>> incidents{};
    double riskTotal = 0;
    double minimum = 0;
    THREAT_LEVEL threatLevel;

public:
    explicit TimelineThreatAnalysis(string employee) :
        employee(std::move(employee)){}

    // utility funcs

    /**
     *  Append factor at given date, and increment risk total
     *  If the factor is enduring, the minimum will be pushed up as well.
     *  todo: Check for duplicates (possibly at the CSV-parsing level)
     */
    void addIncrementFactor(Date date, const Factor* factor){
        riskTotal += factor->getRiskVal();
        incidents.emplace_back(date, factor);
        if (factor->isEnduring())
            minimum += factor->getRiskVal();
    }
    // will hard cap at minimum
    void decrementRisk(double howMuch){
        riskTotal -= howMuch;
        if (riskTotal < minimum)
            riskTotal = minimum;
    }
    // will return 1 if there is only 1 incident, and 0 if there are none
    [[nodiscard]] int getDurationDays(bool toToday = false) const{
        if(incidents.empty())
            return 0;
        else if (incidents.size() == 1 && !toToday)
            return 1;

        Date first = incidents[0].first;
        Date last = toToday ? *new Date : incidents[incidents.size()-1].first;
        return dayDiff(first, last);
    }
    void setThreatLevel(THREAT_LEVEL threatLvl){
        threatLevel = threatLvl;
    }

    // getters
    [[nodiscard]] double getRiskTotal() const{
        return riskTotal;
    }
    [[nodiscard]] const string &getEmployee() const {
        return employee;
    }
    [[nodiscard]] const vector<pair<Date, const Factor *>> &getIncidents() const {
        return incidents;
    }
    [[nodiscard]] THREAT_LEVEL getThreatLevel() const {
        return threatLevel;
    }
};


#endif //OBIT_CLASSIFIER_TIMELINETHREATANALYSIS_H
