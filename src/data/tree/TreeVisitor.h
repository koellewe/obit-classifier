#include <functional>
#include "Node.h"

/**
 * Note to future self: When templating, define everything in the .h file.
 * https://stackoverflow.com/a/648905/3900981
 */

#ifndef OBIT_CLASSIFIER_TREEVISITOR_H
#define OBIT_CLASSIFIER_TREEVISITOR_H

template<class T>
class TreeVisitor {
private:
    Node<T>* rootNode;

    /**
     * Visit the given node, and all its children, going depth-first (DF)
     */
    void visitNodeDF(Node<T>* node, int depth, std::function<void(T*, int)> &callback){
        callback(node->getData(), depth);
        for(auto & child_it : node->getChildren()){
            visitNodeDF(child_it, depth+1, callback);
        }
    }

public:
    explicit TreeVisitor(Node<T>* rootNode){
        this->rootNode = rootNode;
    }

    /**
     * Visit all nodes in the tree, going depth-first.
     * @param callback function that takes a data pointer and the depth as int.
     */
    void visitAll(std::function<void(T*,int)> &callback){
        visitNodeDF(rootNode, 0, callback);
    }

    /**
     *
     * @param callback same as the other visitAll(), but without the depth parameter
     */
    void visitAll(std::function<void(T*)> &callback){
        std::function forwarder = [callback](T* data, int depth){
            callback(data);
        };
        visitAll(forwarder);
    }
};

#endif
