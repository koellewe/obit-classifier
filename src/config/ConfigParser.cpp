
#include "ConfigParser.h"
#include "../util/utils.h"
#include <yaml-cpp/yaml.h>

/**
 * Parses the YAML config and modifies SOFIT in-place accordingly.
 * @param infile config file
 */
void ConfigParser::parseFile(const string &infile) {
    YAML::Node config = YAML::LoadFile(infile);

    if (config["factors"]){
        auto factorsConfig = config["factors"];
        if (!factorsConfig.IsMap()){
            error = "Node [factors] is not in map format.";
            return;
        }

        if(factorsConfig["override_risks"]){
            auto factorsToOverride = factorsConfig["override_risks"];
            if (!factorsToOverride.IsSequence()){
                error = "Subnode [factors.override_risks] is not in sequence format.";
                return;
            }

            bool recursiveOverride = true;
            if (factorsConfig["recursively"]){
                auto recursNode = factorsConfig["recursively"];
                if (!yaml_isBool(recursNode)){
                    error = "Value for [factors.recursively] is not a boolean.";
                    return;
                }
                recursiveOverride = recursNode.as<bool>();
            }

            for(int i = 0; i < factorsToOverride.size(); i++){
                auto factorNode = factorsToOverride[i];
                if (!factorNode.IsMap()){
                    error = "Factor item at index "+std::to_string(i)+" is not a mapping.";
                    return;
                }

                auto factPair = factorNode.begin();
                string factorName = "#" + factPair->first.as<string>();
                if (!sofit->factorExists(factorName)){
                    error = "Specified factor \""+factorName+"\" is not a part of SOFIT.";
                    return;
                }

                if (!yaml_isNumber(factPair->second)){
                    error = "Risk value for specified factor \""+factorName+"\" is not a number.";
                    return;
                }

                int riskVal = factPair->second.as<int>();
                if (riskVal < 0){
                    error = "Risk value for specified factor \""+factorName+"\" is negative. 0 is the minimum.";
                    return;
                }

                sofit->overrideRisk(factorName, riskVal, recursiveOverride);
            }

        }

        // set default risk value
        if(factorsConfig["default_risk"]){
            auto defRiskNode = factorsConfig["default_risk"];
            if (!yaml_isNumber(defRiskNode)){
                error = "[factors.default_risk] is not a number.";
                return;
            }
            int defRisk = defRiskNode.as<int>();
            if (defRisk < 0){
                error = "[factors.default_risk] is negative. 0 is the minimum.";
                return;
            }
            sofit->setDefaultRisk(defRisk);
        }
    }

    if (config["threat_classification"]){
        auto threatClassNode = config["threat_classification"];
        if (!threatClassNode.IsSequence()){
            error = "[threat_classification] is not a sequence.";
            return;
        }
        if (threatClassNode.size() != 4){
            error = "Wrong sequence length for [threat_classification]. Should be exactly 4.";
            return;
        }

        for(int i = 0; i < threatClassNode.size(); i++){
            auto threatNode = threatClassNode[i];
            if (!yaml_isNumber(threatNode)){
                error = "Item at index "+to_string(i)+"of [threat_classification] is not a number.";
                return;
            }

            int i_risk = threatNode.as<int>();

            // only need to perform this check once, for if the first int is positive and the sequence is ascending,
            // then all ints in the sequence are positive.
            if (i==0 && i_risk < 0){
                error = "Item at index 0 of [threat_classification] is negative.";
                return;
            }
            // check that sequence is ascending
            if (i>=1 && i_risk < classificationMatrix[i-1]){
                error = "Sequence at [threat_classification] is not ascending (check index "+to_string(i)+").";
                return;
            }

            // finally, update the matrix
            classificationMatrix[i] = threatNode.as<int>();
        }

    }

    if (config["timeline"]){
        auto timelineNode = config["timeline"];

        if (timelineNode["daily_cooldown"]){
            auto cooldownNode = timelineNode["daily_cooldown"];
            if (!yaml_isFloat(cooldownNode)){
                error = "Value at [timeline.daily_cooldown] is not a number.";
                return;
            }
            cooldown = cooldownNode.as<double>();
        }

        if (timelineNode["run_to_today"]){
            auto rttNode = timelineNode["run_to_today"];
            if (!yaml_isBool(rttNode)){
                error = "Value at [timeline.run_to_today] is not a boolean.";
                return;
            }
            runToToday = rttNode.as<bool>();
        }

        if (timelineNode["enduring_factors"]){
            auto enduranceNode = timelineNode["enduring_factors"];
            if (!enduranceNode.IsSequence()){
                error = "Content at [timeline.enduring_factors] is not a sequence.";
                return;
            }
            for(int i = 0; i < enduranceNode.size(); i++){
                auto subNode = enduranceNode[i];
                if (!subNode.IsScalar()){
                    error = stringprintf("Item index %d of [timeline.enduring_factors] is not a string.", i);
                    return;
                }
                auto factName = "#"+subNode.as<string>();
                if (!sofit->factorExists(factName)){
                    error = stringprintf("Specified factor \"%s\" in [timeline.enduring_factors] index %d does not exist in SOFIT.",
                                         factName, i);
                    return;
                }

                // mutate factor subtree to be enduring
                sofit->setSubtreeEnduring(factName);
            }
        }
    }

    // other config ignored
}
