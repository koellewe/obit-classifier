
#ifndef OBIT_CLASSIFIER_EMPLOYEEPARSER_H
#define OBIT_CLASSIFIER_EMPLOYEEPARSER_H

#include <string>
#include <map>
#include <list>
#include "../util/ErrorProne.h"
#include "SOFITParser.h"

using namespace std;

class EmployeeParser : public ErrorProne{
private:
    map<string, list<string>> emplFactors;
    SOFITParser* sofit;

public:
    explicit EmployeeParser(SOFITParser* sofit){
        this->sofit = sofit;
    };

    void parseFile(const string& infile);

};


#endif //OBIT_CLASSIFIER_EMPLOYEEPARSER_H
