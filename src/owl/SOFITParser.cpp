#include "SOFITParser.h"
#include "../data/tree/TreeVisitor.h"
#include <pugixml.hpp>
#include <map>
#include <list>
#include <functional>
#include <iostream>

using namespace pugi;
using namespace std;

/**
 * Instantiate the parser object by parsing the file.
 * Check isError() afterwards.
 * @param infile SOFIT-OWL file to be parsed
 */
SOFITParser::SOFITParser(const char* infile) {
    xml_document doc;
    xml_parse_result res = doc.load_file(infile);

    if (res.status != xml_parse_status::status_ok){
        error = "Failed to read OWL file: " + std::string(res.description());
        return;
    }

    // will internally set error if apt
    buildSofitTree(doc);
}

/**
 * Iterates over the SOFIT tree and returns all factors that have risk values.
 * @return the map
 */
std::map<string, int> SOFITParser::getFactorRisks() const {
    std::map<string, int> factorMap{};
    std::reference_wrapper<map<string,int>> factorMapRef(factorMap);

    TreeVisitor<Factor> visitor(treeRoot);
    std::function callback = [factorMapRef](Factor* fact)->void {
        if (fact->getRiskVal() > -1){
            factorMapRef.get().insert(pair<string, int>(fact->getIri(), fact->getRiskVal()));
        }
    };
    visitor.visitAll(callback);

    return factorMap;
}

/**
 * Assumes parent data is complete, and uses it to look up children (and their risk vals), and populate the family.
 * Assumes parent has children.
 */
void SOFITParser::populateTree(Node<Factor>* parent, map<string, list<string>>& flatMap, map<string, int>& riskVals){
    string parentIri = parent->getData()->getIri();
    list<string> children = flatMap[parentIri];
    for(auto &child_it : children){
        // create a Factor
        Factor* factor = nullptr;
        if (riskVals.contains(child_it)){
            factor = new Factor(child_it, riskVals[child_it]);
        }else{
            factor = new Factor(child_it);
        }

        // create node, add it to parent family, and to lookup map
        auto node = new Node<Factor>(factor);
        parent->getChildren().push_back(node);
        lookupMap.insert(pair<string, Node<Factor>*>(child_it, node));

        // process child's children if it has any
        if (flatMap.contains(child_it)){
            populateTree(node, flatMap, riskVals);  // so population happens depth-first
        }
    }

}

/**
 * Strategy:
 * Build 2 maps of all subclassings, and of riskValues
 * Find #Factor and build the tree from there
 *
 * This method will set error if something went wrong.
 * @param doc OWL Document to parse
 */
void SOFITParser::buildSofitTree(pugi::xml_document& doc) {

    // map of IRIs, pointing to a list of children.
    // all nodes that have children will appear in the map
    map<string, list<string>> flatMap;

    // map of IRIs pointing to their associated risk value
    std::map<std::string, int> riskValues;

    xml_node ont = doc.child("Ontology");
    for (auto & it : ont.children()){
        if (std::string(it.name()) == "SubClassOf"){ // add to flatMap

            xml_node_iterator inner_it = it.children().begin();
            std::string ontChild = inner_it->attribute("IRI").as_string();
            std::string ontParent = inner_it->next_sibling().attribute("IRI").as_string();

            if (flatMap.contains(ontParent)){ // add child to parent's child-list
                flatMap[ontParent].push_back(ontChild);
            }else{ // add parent and its child to flatMap
                list<string> child_list;
                child_list.push_back(ontChild);
                flatMap.insert(pair(ontParent, child_list));
            }

        }else if (std::string(it.name()) == "AnnotationAssertion"){ // add to riskValues
            xml_node_iterator inner_it = it.children().begin();
            std::string ontProp = inner_it->attribute("IRI").as_string();
            if (ontProp == "#riskValue"){ // factor-risk
                xml_node ontIriNode = inner_it->next_sibling();
                std::string ontIri = ontIriNode.child_value();

                xml_node ontValNode = ontIriNode.next_sibling();
                if (std::string(ontValNode.name()) != "Literal"){
                    error = "Expected int literal in #riskValue AnnotationAssertion. IRI: " + ontIri;
                    return;
                }

                int ontVal = std::stoi(ontValNode.child_value());
                riskValues.insert(std::pair<std::string, int>(ontIri, ontVal));

            }else if (ontProp == "#exhibitsFactor"){ // employee-factor
                xml_node ontEmplNode = inner_it->next_sibling();
                std::string ontEmpl = ontEmplNode.child_value();

                xml_node ontFacNode = ontEmplNode.next_sibling();
                std::string ontFac = ontFacNode.child_value();

                if (employees.contains(ontEmpl)){  // add factor to employee-factor list
                    employees[ontEmpl].push_back(ontFac);
                }else{ // create factor list and add with employee to employee-map
                    list<string> faclist{ontFac,};
                    employees.insert(pair(ontEmpl, faclist));
                }

            }// else ignore
        }else if (std::string(it.name()) == "ClassAssertion"){
            xml_node_iterator inner_it = it.children().begin();
            if(std::string(inner_it->name()) != "Class") continue;
            // add in employee
            if(std::string(inner_it->attribute("IRI").as_string()) == "#Employee"){
                xml_node emplNode = inner_it->next_sibling();
                if (std::string(emplNode.name()) != "NamedIndividual") continue;
                std::string emplName = emplNode.attribute("IRI").as_string();

                if (!employees.contains(emplName)){
                    list<string> faclist{}; // empty list
                    employees.insert(pair(emplName, faclist));
                }

            } // else ignore (if adding in here, be mindful of continue's above)
        }// else ignore
    }
    // initial map-building complete

    // set up top node (will be #Factor)
    string factorName = "#Factor";
    treeRoot = new Node<Factor>(new Factor(factorName));
    lookupMap.insert(pair<string, Node<Factor>*>(factorName, treeRoot));

    // recursively populate tree
    populateTree(treeRoot, flatMap, riskValues);

}

bool SOFITParser::factorExists(string &check) const {
    return lookupMap.contains(check);
}

/**
 * Get a map from employee IRI -> list of Factors they exhibit
 */
map<string, list<const Factor *>> SOFITParser::getEmployeeFactors() const{
    map<string, list<const Factor *>> emplList;
    for(auto& emplPair : employees){
        list<const Factor*> facs;
        for (auto& facName : emplPair.second){  // iterate over factorNames exhibited by the employee
            // look up factor in tree, and add it to list
            facs.push_back(lookupMap.at(facName)->getData());
        }
        // add employee IRI together with Factor list
        emplList.insert(pair(emplPair.first, facs));
    }
    return emplList;
}

/**
 * Overrides the specified factor's risk value
 * @param factor IRI. Must exist. Use factorExists() first.
 * @param riskVal the new risk value
 * @param recursively if true, will also override all the node's children.
 */
void SOFITParser::overrideRisk(string &factor, int riskVal, bool recursively) {
    lookupMap[factor]->getData()->setRiskVal(riskVal);
    if (recursively){
        TreeVisitor visitor(lookupMap[factor]);
        std::function callback = [riskVal](Factor* f) -> void {
            f->setRiskVal(riskVal);
        };
        visitor.visitAll(callback);
    }
}

/**
 * When done adding all employees, run validate().
 */
void SOFITParser::addEmployee(string &name, list<string>& factors) {
    employees.insert(pair(name, factors));
}

void SOFITParser::printTree() const {
    TreeVisitor visitor(treeRoot);
    std::function callback = [](Factor* fact, int depth) -> void {
        for(int i = 0; i < depth; i++){  // display depth as number of preceding ='s
            cout << "= ";
        }
        cout << fact->getIri() << " | ";
        if (fact->getRiskVal() >= 0)
            cout << fact->getRiskVal();
        cout << endl;
    };
    visitor.visitAll(callback);
}

/**
 * Sets all unassigned risk values to the specified default.
 * Note, this method only works once.
 * @param defRisk default risk value
 */
void SOFITParser::setDefaultRisk(int defRisk) {
    TreeVisitor visitor(treeRoot);
    std::function callback = [defRisk](Factor* fact) -> void {
        if (fact->getRiskVal() == -1)
            fact->setRiskVal(defRisk);
    };
    visitor.visitAll(callback);
}

/**
 * Checks whether SOFIT is valid. Good idea to call this after making changes to the object.
 * Result in isError().
 */
void SOFITParser::validate() {
    // check employees
    for(auto& empl : employees){
        string emplName = empl.first;
        for(auto& fact : empl.second){
            if (lookupMap.at(fact)->getData()->getRiskVal() == -1){
                error = "Employee \""+emplName+"\" exhibits factor \"";
                error.append(fact).append("\", which does not have a risk value assigned.");
                return;
            }
        }
    }

}

/**
 * Assumes you've already checked factorExists().
 */
const Factor *SOFITParser::getFactor(const string &fact) const {
    return lookupMap.at(fact)->getData();
}

/**
 * Mutate a subtree's factors and set them all to be enduring.
 * Assumes topFactor exists.
 */
void SOFITParser::setSubtreeEnduring(const string& topFact) {
    TreeVisitor visitor(lookupMap[topFact]);
    std::function callback = [](Factor* fact) -> void {
        fact->setEnduring();
    };
    visitor.visitAll(callback);
}
