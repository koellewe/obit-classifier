
#ifndef OBIT_CLASSIFIER_TIMELINEPARSER_H
#define OBIT_CLASSIFIER_TIMELINEPARSER_H


#include "../util/ErrorProne.h"
#include "ThreatClassifier.h"
#include <string>
#include <map>
#include <ctime>
#include "../util/Date.h"

using namespace std;

class TimelineParser : public ErrorProne{
private:
    // map from date -> list of empl-factor pairs
    map<Date, list<pair<string, const Factor*>>> timelineData;

    // helper methods
    void parseFile(SOFITParser* sofit, const string& infile);

public:
    /**
     * Parses the infile, but does not run classification yet.
     * Check isError() after construction.
     * @param classifier
     * @param infile
     */
    explicit TimelineParser(SOFITParser* sofit, const string& infile){
        parseFile(sofit, infile);
    }

    [[nodiscard]] const map<Date, list<pair<string, const Factor *>>> &getTimelineData() const {
        return timelineData;
    }

    /**
     * Expensive method that groups timeline data by employee
     */
    [[nodiscard]] map<string, list<pair<Date, const Factor*>>> getTimelineByEmployee() const;

};


#endif //OBIT_CLASSIFIER_TIMELINEPARSER_H
