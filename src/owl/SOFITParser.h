#include <string>
#include <map>
#include <pugixml.hpp>
#include "../data/tree/Node.h"
#include "../data/sofit/Factor.h"
#include "../util/ErrorProne.h"
#include <list>

#ifndef OBIT_CLASSIFIER_SOFIT_PARSER_H
#define OBIT_CLASSIFIER_SOFIT_PARSER_H

using namespace std;

class SOFITParser : public ErrorProne{
private:
    Node<Factor>* treeRoot = nullptr;
    std::map<std::string, Node<Factor>*> lookupMap{};
    std::map<string, list<string>> employees;

    // internal utility fns
    void buildSofitTree(pugi::xml_document& doc);
    void populateTree(Node<Factor>* parent, map<string, list<string>>& flatMap, map<string, int>& riskVals);

public:
    explicit SOFITParser(const char* infile);

    // utility functions
    bool factorExists(string& check) const;
    [[nodiscard]] const Factor* getFactor(const string& fact) const;
    [[nodiscard]] map<string,int> getFactorRisks() const;
    [[nodiscard]] map<string, list<const Factor*>> getEmployeeFactors() const;
    void validate();

    // modifiers
    void overrideRisk(string& factor, int riskVal, bool recursively = true);
    void setSubtreeEnduring(const string& topFactor);
    void addEmployee(string& name, list<string>& factors);
    void setDefaultRisk(int defRisk);

    // vanity
    void printTree() const;

    //destructor
    ~SOFITParser(){
        // deleting a nullptr has no effect
        delete treeRoot; // will recursively delete nodes
    }
};

#endif
