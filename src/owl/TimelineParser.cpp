
#include "TimelineParser.h"
#include <fstream>
#include "../util/utils.h"

using namespace std;

// Parse timeline file
void TimelineParser::parseFile(SOFITParser* sofit, const string &infileName) {
    ifstream infile;
    infile.open(infileName, ios::in);
    if (!infile.is_open()){
        error = "Failed to open timeline file \""+infileName+"\".";
        return;
    }
    string line;
    while(getline(infile, line)) {
        if (line.empty()) continue;  // ignore empty lines

        auto splat = strsplit(line, ',');
        if (splat.size() != 3){
            error = "Malformed line in \""+infileName;
            error.append("\": ").append(line);
            return;
        }

        // parse date
        Date date(splat[0]);
        string& emplName = splat[1];
        string& factName = splat[2];
        if(!sofit->factorExists(factName)){
            error = "Specified factor \"" + factName + "\" missing in SOFIT.";
            return;
        }
        auto fact = sofit->getFactor(factName);
        if (fact->getRiskVal() == -1){
            error = "Specified factor \""+factName+"\" for employee \"";
            error.append(emplName).append("\" does not have an associated risk value.");
            return;
        }

        if(timelineData.contains(date)){
            timelineData[date].push_back(pair(emplName, fact));
        }else{
            timelineData.insert(pair(date, *new list{pair(emplName, fact)}));
        }
    }
    infile.close();
}

map<string, list<pair<Date, const Factor *>>> TimelineParser::getTimelineByEmployee() const {
    map<string, list<pair<Date, const Factor*>>> timelineByEmpl;
    // timelineData is guaranteed to be ordered by date (internal BST)
    for(auto& datePair : timelineData){
        const Date& the_date = datePair.first;

        for(auto& emplPair : datePair.second){
            const string& empl = emplPair.first;
            auto fact = emplPair.second;

            // all lists auto-instantiated
            timelineByEmpl[empl].push_back(pair(the_date, fact));
        }

    }

    return timelineByEmpl;
}
