
#include "ThreatClassifier.h"
#include "../util/utils.h"

using namespace std;

void ThreatClassifier::classifyThreats() {
    auto employees = sofit->getEmployeeFactors();

    for(auto& emplPair : employees){
        string emplName = emplPair.first;
        list factors = emplPair.second;
        int riskTotal = 0;
        for(auto& factor: factors){
            riskTotal += factor->getRiskVal();
        }

        THREAT_LEVEL classification = classifyRisk(riskTotal);

        // bag employee
        emplClassifications.insert(
                pair(emplName, new ThreatAnalysis(emplName, factors, riskTotal, classification))
                );
    }
}

ThreatAnalysis::THREAT_LEVEL ThreatClassifier::classifyRisk(double riskVal) {

    for(int i = 0; i < CLASS_MATRIX_LEN; i++){
        if (riskVal <= classificationMatrix[i])
            return ThreatAnalysis::THREAT_LEVEL_ARR[i];
    }
    // else riskVal > all class matrix elements
    return THREAT_LEVEL::EXTREME;

}

// Classify full employee timeline
TimelineThreatAnalysis
ThreatClassifier::classifyTimeline(const string &empl, list<pair<Date, const Factor *>> dateFactors) {
    TimelineThreatAnalysis anal(empl);

    auto datePair_it = dateFactors.begin();
    // first factor counts 100%
    anal.addIncrementFactor(datePair_it->first, datePair_it->second);

    Date* lastIncident = &datePair_it->first;
    datePair_it++;
    while(datePair_it != dateFactors.end()){
        int daysPassed = dayDiff(*lastIncident, datePair_it->first);

        anal.decrementRisk(daysPassed * dailyCooldown);
        anal.addIncrementFactor(datePair_it->first, datePair_it->second);

        lastIncident = &datePair_it->first;
        datePair_it++;
    }

    // conditionally classify to today and then classify final risk total
    classifyToToday(&anal, *lastIncident);

    return anal;
}

/**
 * Update an anal object, and reclassify it.
 */
void ThreatClassifier::classifyRunningTimeline(TimelineThreatAnalysis *anal, Date the_date, const Factor *fact, int daysSinceLast) {

    if (daysSinceLast > 0)
        anal->decrementRisk(daysSinceLast * dailyCooldown);

    anal->addIncrementFactor(the_date, fact);

    anal->setThreatLevel(classifyRisk(anal->getRiskTotal()));

}

/**
 * If runToToday is specified in config, this method will decrease the risk total by the appropriate amount
 * from the analysis's last incident to today. Will then unconditionally do a final classification of the anal.
 */
void ThreatClassifier::classifyToToday(TimelineThreatAnalysis *anal, Date& lastIncident) {
    if (runToToday){
        // calculate days from last incident to today
        int daysSinceLast = dayDiff(lastIncident, *new Date);
        anal->decrementRisk(daysSinceLast * dailyCooldown);
    }

    anal->setThreatLevel(classifyRisk(anal->getRiskTotal()));
}
