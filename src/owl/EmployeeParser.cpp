
#include "EmployeeParser.h"
#include <fstream>
#include <string>
#include "../util/utils.h"

using namespace std;

/**
 * Parses an employee csv file.
 * Check isError() afterwards.
 * @param infileName
 */
void EmployeeParser::parseFile(const string &infileName) {
    ifstream infile;
    infile.open(infileName, ios::in);
    if (!infile.is_open()){
        error = "Failed to open employees file \""+infileName+"\".";
        return;
    }
    string line;
    while(getline(infile, line)){
        if (line.empty()) continue;  // ignore empty lines

        auto splat = strsplit(line, ',');
        string emplName = splat[0];
        if (emplName.length() < 2 || emplName[0] != '#'){
            error = "Invalid employee ID \""+emplName+"\".";
            return;
        }

        list<string> factors; // -1 to exclude employee name
        for(int i = 1; i < splat.size(); i++){
            if (!sofit->factorExists(splat[i])){
                error = "Specified factor \""+splat[i]+"\" is not defined in SOFIT.";
                return;
            }
            factors.push_back(splat[i]);
        }

        sofit->addEmployee(emplName, factors);
    }
    infile.close();
}
