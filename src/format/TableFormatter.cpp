
#include "TableFormatter.h"

#include <iostream>

using namespace std;

void TableFormatter::formatAndPrint() {
    int rowWidth = 0;
    // print headers
    for(auto& headerPair : headers){
        string headerName = headerPair.first;
        int colWidth = headerPair.second;
        rowWidth += colWidth;

        int spacePadding = colWidth-headerName.size();
        cout << headerName;
        for(int i = 0; i < spacePadding; i++)
            cout << " ";
        if (headerPair != *--headers.end()) { // skip spaces at last col
            cout << "  ";  // col spacing
            rowWidth += 2;
        }
    }
    cout << endl; // finish top-row header
    for(auto& headerPair : headers){
        int colWidth = headerPair.second;
        for(int i = 0; i < colWidth; i++){
            cout << "=";
        }
        if (headerPair != *--headers.end()) // skip spaces at last col
            cout << "  ";
    }
    cout << endl; // finish second-row of header

    // print data
    int row_i = 0;
    int sep_i = 0;
    for(auto& row : data){
        // print seperator line if necessary
        if (sep_i < seps.size() && row_i == seps[sep_i]){
            for(int i = 0; i < rowWidth; i++)
                cout << "-";
            cout << endl;
            sep_i++;
        }

        // print row
        for(int el_i = 0; el_i < row.size(); el_i++){
            string el = row.at(el_i);
            int colWidth = headers.at(el_i).second;
            if (el.size() > colWidth){ // truncate el data
                el = el.substr(0, colWidth - 3) + "...";
                cout << el;
            }else{
                cout << el;
                int padding = colWidth - el.size();
                for(int i = 0; i < padding; i++){
                    cout << " ";
                }
            }
            if (el_i < row.size()-1)  // skip col spacing at last col
                cout << "  ";
        }
        cout << endl; // end row
        row_i++;
    }

}
