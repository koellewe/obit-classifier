
#ifndef OBIT_CLASSIFIER_TABLEFORMATTER_H
#define OBIT_CLASSIFIER_TABLEFORMATTER_H

#include <list>
#include <vector>
#include <string>

using namespace std;

class TableFormatter {
private:
    const vector<pair<string, int>> headers;
    const list<vector<string>> data;
    vector<int> seps;

public:
    /**
     * @param headers list of pairs containing the header name and its width
     * @param data 2d list of data items (all as strings)
     */
    TableFormatter(const vector<pair<string, int>> &headers, const list<vector<string>> &data) :
        headers(headers), data(data){}

    /**
     * List of row indices before which I should print a separator line.
     */
    void setSeparatorIndices(const vector<int>& sepIdx){
        this->seps = sepIdx;
    }

    void formatAndPrint();
};


#endif //OBIT_CLASSIFIER_TABLEFORMATTER_H
