
#ifndef OBIT_CLASSIFIER_CSVFORMATTER_H
#define OBIT_CLASSIFIER_CSVFORMATTER_H

#include <list>
#include <string>

using namespace std;

class CSVFormatter {
private:
    list<string> headers;
    list<list<string>> data;

public:
    CSVFormatter(const list<string> &headers, const list<list<string>> &data) : headers(headers), data(data) {}

    void formatAndPrint();

};


#endif //OBIT_CLASSIFIER_CSVFORMATTER_H
