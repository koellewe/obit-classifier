
#include "EmployeeParserTestSuite.h"
#include "../../src/util/utils.h"

TEST_F(EmployeeParserTestSuite, employeesTest){
    auto empls = sofit->getEmployeeFactors();
    ASSERT_EQ(12, empls.size()) << "Big 5, 2 duds, and 5 extra from config";

    ASSERT_TRUE(empls.contains("#Meyer"));
    auto meyerFacts = empls.at("#Meyer");
    EXPECT_EQ(2, meyerFacts.size());
    EXPECT_TRUE(containsFactor(meyerFacts, "#Job_Search"));
    EXPECT_TRUE(containsFactor(meyerFacts, "#Using_Social_Media"));

    ASSERT_TRUE(empls.contains("#January"));
    EXPECT_EQ(0, empls.at("#January").size());

    ASSERT_TRUE(empls.contains("#Van_der_Merwe"));
    EXPECT_EQ(1, empls.at("#Van_der_Merwe").size());
    ASSERT_TRUE(containsFactor(empls.at("#Van_der_Merwe"), "#New-Hire"));
}
