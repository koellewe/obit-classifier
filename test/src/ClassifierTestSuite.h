
#ifndef OBIT_CLASSIFIER_CLASSIFIERTESTSUITE_H
#define OBIT_CLASSIFIER_CLASSIFIERTESTSUITE_H

#include <gtest/gtest.h>
#include "../../src/owl/SOFITParser.h"
#include "../../src/config/ConfigParser.h"
#include "../../src/owl/ThreatClassifier.h"

class ClassifierTestSuite : public ::testing::Test{
public:
    SOFITParser* sofit;
    ThreatClassifier* classifier;

    void SetUp() override{
        sofit = new SOFITParser("test/res/empl5.sofit.owl");
        // init with empty config
        classifier = new ThreatClassifier(sofit, *new ConfigParser(sofit));
    }

};


#endif //OBIT_CLASSIFIER_CLASSIFIERTESTSUITE_H
