
#include "ClassifierTestSuite.h"
#include "../../src/owl/EmployeeParser.h"
#include <gtest/gtest.h>
#include <list>

using namespace std;

TEST_F(ClassifierTestSuite, big5Test){
    classifier->classifyThreats();
    auto result = classifier->getEmplClassifications();

    ASSERT_TRUE(result.contains("#Snowden"));
    auto snowdenThreat = result.at("#Snowden");
    EXPECT_EQ("#Snowden", snowdenThreat->getEmployee());
    EXPECT_EQ(ThreatAnalysis::THREAT_LEVEL::EXTREME, snowdenThreat->getThreatLevel());
    EXPECT_EQ(488, snowdenThreat->getRiskTotal());
    EXPECT_TRUE(containsFactor(snowdenThreat->getFactors(), "#Resigned_To_Take_Another_Job"));
    EXPECT_FALSE(containsFactor(snowdenThreat->getFactors(), "#Radical_Political_Beliefs"));

    ASSERT_TRUE(result.contains("#Doe"));
    auto doeThreat = result.at("#Doe");
    EXPECT_EQ("#Doe", doeThreat->getEmployee());
    EXPECT_EQ(ThreatAnalysis::THREAT_LEVEL::MEDIUM, doeThreat->getThreatLevel());
    EXPECT_EQ(30, doeThreat->getRiskTotal());
    EXPECT_EQ(1, doeThreat->getFactors().size());
    EXPECT_TRUE(containsFactor(doeThreat->getFactors(), "#Odd_Hours_Work_Week"));
    EXPECT_FALSE(containsFactor(doeThreat->getFactors(), "#Distorted_Perceptions"));

    ASSERT_TRUE(result.contains("#McEmpty"));
    auto emptyThreat = result.at("#McEmpty");
    EXPECT_EQ("#McEmpty", emptyThreat->getEmployee());
    EXPECT_EQ(ThreatAnalysis::THREAT_LEVEL::NONE, emptyThreat->getThreatLevel());
    EXPECT_EQ(0, emptyThreat->getRiskTotal());
    EXPECT_EQ(0, emptyThreat->getFactors().size());
}

TEST_F(ClassifierTestSuite, moreEmployeesTest){
    EmployeeParser empls(sofit);
    empls.parseFile("test/res/more_employees.csv");
    classifier->classifyThreats();
    auto result = classifier->getEmplClassifications();

    ASSERT_TRUE(result.contains("#Van_der_Merwe"));
    auto vdmThreat = result.at("#Van_der_Merwe");
    EXPECT_EQ("#Van_der_Merwe", vdmThreat->getEmployee());
    EXPECT_EQ(ThreatAnalysis::THREAT_LEVEL::MEDIUM, vdmThreat->getThreatLevel());
    EXPECT_EQ(34, vdmThreat->getRiskTotal());
    EXPECT_TRUE(containsFactor(vdmThreat->getFactors(), "#New-Hire"));
    EXPECT_FALSE(containsFactor(vdmThreat->getFactors(), "#Using_Social_Media"));

    ASSERT_TRUE(result.contains("#January"));
    auto janThreat = result.at("#January");
    EXPECT_EQ(ThreatAnalysis::THREAT_LEVEL::NONE, janThreat->getThreatLevel());
    EXPECT_EQ(0, janThreat->getFactors().size());
    EXPECT_EQ(0, janThreat->getRiskTotal());
}
