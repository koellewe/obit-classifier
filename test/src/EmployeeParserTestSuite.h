
#ifndef OBIT_CLASSIFIER_EMPLOYEEPARSERTESTSUITE_H
#define OBIT_CLASSIFIER_EMPLOYEEPARSERTESTSUITE_H

#include <gtest/gtest.h>
#include "../../src/owl/SOFITParser.h"
#include "../../src/owl/EmployeeParser.h"

class EmployeeParserTestSuite : public ::testing::Test{
public:
    SOFITParser* sofit;
    EmployeeParser* emplParser;

    void SetUp() override{
        sofit = new SOFITParser("test/res/empl5.sofit.owl");
        emplParser = new EmployeeParser(sofit);
        emplParser->parseFile("test/res/more_employees.csv");

        ASSERT_FALSE(emplParser->isError()) << emplParser->getResetError();
    }

};


#endif //OBIT_CLASSIFIER_EMPLOYEEPARSERTESTSUITE_H
