#include <gtest/gtest.h>
#include "../../src/owl/SOFITParser.h"
#include "SofitTestSuite.h"
#include "../../src/util/utils.h"

TEST_F(SofitTestSuite, factorRisksTest){
    auto risks = parser.getFactorRisks();
    EXPECT_EQ(247, risks.size()) << "Bad number of risk-bearing factors parsed.";

    ASSERT_TRUE(risks.contains("#Credit_Problems")) << "Factor #Credit_Problems missing in factor-risk map.";
    EXPECT_EQ(67, risks.at("#Credit_Problems")) << "Bad risk value for #Credit_Problems.";

    //leaf
    ASSERT_TRUE(risks.contains("#Excessive_Browsing_To_Non-Work_Related_Websites")) << "Factor #Excessive_Browsing_To_Non-Work_Related_Websites missing in factor-risk map.";
    EXPECT_EQ(33, risks.at("#Excessive_Browsing_To_Non-Work_Related_Websites")) << "Bad risk value for #Excessive_Browsing_To_Non-Work_Related_Websites";

    // should not contain non-risk-val-factors
    EXPECT_FALSE(risks.contains("#Individual_Factor")) << "Factor #Individual_Factor present in factor-risk map, though it should have no risk-val.";
}

TEST_F(SofitTestSuite, employeeFactorTest){
    auto employees = parser.getEmployeeFactors();
    EXPECT_EQ(7, employees.size()) << "Bad number of test-case employees.";

    ASSERT_TRUE(employees.contains("#Snowden")) << "Snowden not in employee-factor map.";
    auto snowdenFactors = employees["#Snowden"];
    EXPECT_EQ(7, snowdenFactors.size()) << "Bad number of factors for #Snowden.";

    EXPECT_TRUE(containsFactor(snowdenFactors, "#Extreme_Discontent"));
    EXPECT_FALSE(containsFactor(snowdenFactors, "#Credit_Problems_Due_To_Overspending"));
}
