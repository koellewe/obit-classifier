
#ifndef OBIT_CLASSIFIER_TIMELINEPARSERTESTSUITE_H
#define OBIT_CLASSIFIER_TIMELINEPARSERTESTSUITE_H

#include <gtest/gtest.h>
#include "../../src/owl/TimelineParser.h"

class TimelineParserTestSuite : public ::testing::Test{
public:
    TimelineParser* parser;

    void SetUp() override{
        auto* sofit = new SOFITParser("sofit.owl");
        ASSERT_FALSE(sofit->isError()) << sofit->getResetError();
        auto* cfg = new ConfigParser(sofit);
        cfg->parseFile("test/res/config2.yml");
        ASSERT_FALSE(cfg->isError()) << cfg->getResetError();

        parser = new TimelineParser(sofit, "test/res/timeline.csv");
        ASSERT_FALSE(parser->isError()) << parser->getResetError();
    }
};


#endif //OBIT_CLASSIFIER_TIMELINEPARSERTESTSUITE_H
