#include <vector>
#include <string>
#include <iostream>
#include "TreeVisitorTestSuite.h"

using namespace std;

const vector<const char*> TreeVisitorTestSuite::expected{"node0", "node1", "node2", "node3"};

TEST_F(TreeVisitorTestSuite, depthFirstTest){
    std::vector<const char*> actual;
    std::reference_wrapper<vector<const char*>> actualRef(actual);

    std::function callback = [actualRef](const char* val) -> void {
        actualRef.get().push_back(val);
    };
    visitor->visitAll(callback);

    EXPECT_EQ(expected.size(), actual.size()) << "Wrong number of nodes visited.";
    for (int i = 0; i < expected.size(); ++i){
        std::string exStr = expected[i];
        std::string acStr = actual[i];
        EXPECT_EQ(acStr, exStr) << "Bad node visitation order at index " << i << ".";
    }
}
