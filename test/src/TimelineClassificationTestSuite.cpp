
#include "TimelineClassificationTestSuite.h"

TEST_F(TimelineClassificationTestSuite, timelineClassificationTest){
    auto empls = timeline->getTimelineByEmployee();

    auto davidson = classifier->classifyTimeline("#Davidson", empls.at("#Davidson"));

    EXPECT_EQ("#Davidson", davidson.getEmployee());
    EXPECT_EQ(1, davidson.getDurationDays());
    EXPECT_EQ(17, davidson.getRiskTotal());
    auto incidents = davidson.getIncidents();
    EXPECT_EQ(1, incidents.size());
    auto incident = incidents.begin();
    EXPECT_EQ(*new Date("2021-01-04"), incident->first);
    EXPECT_EQ("#Absences", incident->second->getIri());

    auto meyer = classifier->classifyTimeline("#Meyer", empls.at("#Meyer"));
    EXPECT_EQ(11, meyer.getDurationDays());
    EXPECT_EQ(85.2, meyer.getRiskTotal());
    EXPECT_EQ(3, meyer.getIncidents().size());

}

TEST_F(TimelineClassificationTestSuite, timelineTest){
    auto empls = timeline->getTimelineByEmployee();

    auto meyer = empls.at("#Meyer");
    TimelineThreatAnalysis anal("#Meyer");

    auto inc_it = meyer.begin();
    classifier->classifyRunningTimeline(&anal, inc_it->first, inc_it->second);
    EXPECT_EQ(44, anal.getRiskTotal());
    EXPECT_EQ(THREAT_LEVEL::LOW, anal.getThreatLevel());

    inc_it++;
    classifier->classifyRunningTimeline(&anal, inc_it->first, inc_it->second, 5);
    EXPECT_EQ(62, anal.getRiskTotal());
    EXPECT_EQ(THREAT_LEVEL::MEDIUM, anal.getThreatLevel());

    inc_it++;
    classifier->classifyRunningTimeline(&anal, inc_it->first, inc_it->second, 6);
    EXPECT_EQ(85.2, anal.getRiskTotal());
    EXPECT_EQ(THREAT_LEVEL::HIGH, anal.getThreatLevel());

}

TEST_F(TimelineClassificationTestSuite, enduringTest){
    // Set up own mini-fixture
    ConfigParser cfg(sofit);
    cfg.parseFile("test/res/config-enduring.yml");
    classifier = new ThreatClassifier(sofit, cfg);
    timeline = new TimelineParser(sofit, "test/res/timeline-enduring.csv");
    ASSERT_FALSE(timeline->isError()) << timeline->getResetError();

    auto dateFactors = timeline->getTimelineByEmployee().at("#Zuma");
    TimelineThreatAnalysis anal("#Zuma");

    auto inc_it = dateFactors.begin();
    classifier->classifyRunningTimeline(&anal, inc_it->first, inc_it->second);
    EXPECT_EQ(34, anal.getRiskTotal()) << "#New-Hire";

    inc_it++;
    classifier->classifyRunningTimeline(&anal, inc_it->first, inc_it->second, 7);
    EXPECT_EQ(94, anal.getRiskTotal()) << "#Grandiosity (should endure)";

    inc_it++;
    classifier->classifyRunningTimeline(&anal, inc_it->first, inc_it->second, 417);
    EXPECT_EQ(136, anal.getRiskTotal()) << "#Disciplinary_Action (should cool down)";

    inc_it++;
    classifier->classifyRunningTimeline(&anal, inc_it->first, inc_it->second, 465);
    EXPECT_EQ(144, anal.getRiskTotal()) << "#Disloyal_Sympathies (should endure)";

    inc_it++;
    classifier->classifyRunningTimeline(&anal, inc_it->first, inc_it->second, 411);
    EXPECT_EQ(213, anal.getRiskTotal()) << "Final total is mix of enduring and cooling-down factors.";

    // also check static timeline classification
    auto staticAnal = classifier->classifyTimeline("#Zuma", dateFactors);
    EXPECT_EQ(213, staticAnal.getRiskTotal());
}
